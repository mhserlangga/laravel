<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function table(){
        return view('latihan.table');
    }

    public function datatable(){
        return view('latihan.data-table');
    }
}
